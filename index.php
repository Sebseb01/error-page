<!DOCTYPE HTML>
<html lang="fr-FR">
  <head>
    <title>SwissLinux.org</title>
    <meta name="author" content="Sébastien Gendre" />
    <meta name="description" content="Site en reconstruction" />
    <link rel="stylesheet" href="slo.css" type="text/css" media="screen" />
    <meta charset="UTF-8"/>
  </head>
  <body>
    <div id="main">
      <div id="image">
	<img src="gentleman-penguin.svg" alt="Pinguin Gentleman" />
      </div>
      <div id="text">
	<p>Une erreur est survenue.
	Nous travaillons dessus.</p>
      <p>Nous serons de retour incessamment sous peu.</p>
      </div>
    </div>
  </body>
</html>
